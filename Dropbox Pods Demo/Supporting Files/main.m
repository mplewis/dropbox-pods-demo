//
//  main.m
//  Dropbox Pods Demo
//
//  Created by Matthew Lewis on 6/13/14.
//  Copyright (c) 2014 Kestrel Development. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DPDAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([DPDAppDelegate class]));
    }
}
