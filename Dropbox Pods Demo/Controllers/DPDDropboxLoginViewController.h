//
//  DPDDropboxLoginViewController.h
//  Dropbox Pods Demo
//
//  Created by Matthew Lewis on 6/13/14.
//  Copyright (c) 2014 Kestrel Development. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <Dropbox/Dropbox.h>

@interface DPDDropboxLoginViewController : UIViewController

@property (nonatomic, retain) IBOutlet UIButton *linkUnlinkButton;
@property (nonatomic, retain) IBOutlet UILabel *currentUsernameLabel;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *continueButton;

@end
