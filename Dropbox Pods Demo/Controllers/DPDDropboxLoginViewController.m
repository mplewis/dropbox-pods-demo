//
//  DPDDropboxLoginViewController.m
//  Dropbox Pods Demo
//
//  Created by Matthew Lewis on 6/13/14.
//  Copyright (c) 2014 Kestrel Development. All rights reserved.
//

#import "DPDDropboxLoginViewController.h"

#import <Dropbox/Dropbox.h>

@interface DPDDropboxLoginViewController ()

@end

@implementation DPDDropboxLoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    DBAccountManager *mgr = [DBAccountManager sharedManager];
    [mgr addObserver:@"acctWatch" block:^(DBAccount *account) {
        [self checkIfAccountLinked];
    }];
    
    [self checkIfAccountLinked];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)checkIfAccountLinked
{
    DBAccountManager *mgr = [DBAccountManager sharedManager];
    DBAccount *acct = [mgr linkedAccount];

    if (!acct) {
        [self.linkUnlinkButton setTitle:@"Link to Dropbox" forState:UIControlStateNormal];
        [self.continueButton setEnabled:NO];
        [self.currentUsernameLabel setText:@""];
    } else {
        [self.linkUnlinkButton setTitle:@"Unlink from Dropbox" forState:UIControlStateNormal];
        [self.continueButton setEnabled:YES];
        [acct addObserver:@"infoWatch" block:^{
            DBAccount *acct = [mgr linkedAccount];
            DBAccountInfo *info = [acct info];

            if (info) {
                [self.currentUsernameLabel setText:[info displayName]];
            }
        }];
    }
}

- (IBAction)didPressLinkUnlink
{
    DBAccountManager *mgr = [DBAccountManager sharedManager];
    
    if ([mgr linkedAccount]) {
        [[mgr linkedAccount] unlink];
    } else {
        [mgr linkFromController:self];
    }
}

@end
