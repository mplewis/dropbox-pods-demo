//
//  DPDAppDelegate.h
//  Dropbox Pods Demo
//
//  Created by Matthew Lewis on 6/13/14.
//  Copyright (c) 2014 Kestrel Development. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DPDAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
